#include "texture.h"
#include <assert.h>
#include <d3dx9.h>
#ifdef _MSC_VER
#	include <DxErr.h>
#else
#	include <dxerr9.h>
#endif

template<typename T> bool safe_release( T &d ) {
	if ( d != nullptr ) {
		d->Release();
		d = nullptr;
		return true;
	}

	return false;
}

SRTexture::SRTexture( int width, int height ) {
	textureSize = { width, height };
	isReleased = true;
	isRendered = false;
	isRenderToTexture = false;
#if __has_include( <d3dx9.h>)
	render = new CD3DRender( 256 );
#endif
	// 	textureIsInit = false;
	pTexture = nullptr;
	PP1S = nullptr;
	DS = nullptr;
	_rz = 0.0f;
}

SRTexture::~SRTexture() {
	if ( !isReleased ) { Invalidate(); }
#if __has_include( <d3dx9.h>)
	delete render;
#endif

	//	safe_release( pTexture );
}

void SRTexture::Invalidate() {
	if ( isReleased ) { return; }

	if ( isRenderToTexture ) { End(); }

	safe_release( DS );
	safe_release( pTexture );

#if __has_include( <d3dx9.h>)
	render->Invalidate();
#endif
	isReleased = true;
	// 	textureIsInit = false;

	isRendered = false;
}

void SRTexture::Initialize( IDirect3DDevice9 *pDevice ) {
	if ( pDevice != nullptr ) { this->pDevice = pDevice; }

	if ( !isReleased ) { return; }

	isReleased = false;
#if __has_include( <d3dx9.h>)
	render->Initialize( this->pDevice );
#endif

	this->pDevice
		->CreateTexture( textureSize.x, textureSize.y, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &pTexture, NULL );

	pTexture->GetSurfaceLevel( 0, &PP1S );
	PP1S->Release();

	this->pDevice->CreateDepthStencilSurface( textureSize.x, textureSize.y, D3DFMT_D24X8, D3DMULTISAMPLE_NONE, 0, FALSE, &DS, NULL );

	isRendered = false;
}

#if __has_include( <d3dx9.h>)
bool SRTexture::Draw( IDirect3DTexture9 *texture, int X, int Y, int W, int H ) {
	if ( isReleased ) { return false; }

	if ( W == 0 ) { W = textureSize.x; }

	if ( H == 0 ) { H = textureSize.y; }

	render->D3DBindTexture( texture );
	if ( SUCCEEDED( render->Begin( D3DPT_TRIANGLELIST ) ) ) {
		render->D3DColor( eCdWhite );

		// I
		float dX = 0, dY = 0;
		float cX = W / 2, cY = H / 2;
		float Xn = X, Yn = Y;

		Xn = X + cX + ( dX - cX ) * cosf( _rz ) - ( dY - cY ) * sinf( _rz );
		Yn = Y + cY + ( dX - cX ) * sinf( _rz ) + ( dY - cY ) * cosf( _rz );

		render->D3DTexCoord2f( 0.0, 0.0 );
		render->D3DVertex2f( Xn, Yn );

		dX = W;
		dY = 0;

		Xn = X + cX + ( dX - cX ) * cosf( _rz ) - ( dY - cY ) * sinf( _rz );
		Yn = Y + cY + ( dX - cX ) * sinf( _rz ) + ( dY - cY ) * cosf( _rz );

		render->D3DTexCoord2f( 1.0, 0.0 );
		render->D3DVertex2f( Xn, Yn );

		dX = 0;
		dY = H;

		Xn = X + cX + ( dX - cX ) * cosf( _rz ) - ( dY - cY ) * sinf( _rz );
		Yn = Y + cY + ( dX - cX ) * sinf( _rz ) + ( dY - cY ) * cosf( _rz );

		render->D3DTexCoord2f( 0.0, 1.0 );
		render->D3DVertex2f( Xn, Yn );

		// II
		dX = 0;
		dY = H;

		Xn = X + cX + ( dX - cX ) * cosf( _rz ) - ( dY - cY ) * sinf( _rz );
		Yn = Y + cY + ( dX - cX ) * sinf( _rz ) + ( dY - cY ) * cosf( _rz );

		render->D3DTexCoord2f( 0.0, 1.0 );
		render->D3DVertex2f( Xn, Yn );

		dX = W;
		dY = 0;

		Xn = X + cX + ( dX - cX ) * cosf( _rz ) - ( dY - cY ) * sinf( _rz );
		Yn = Y + cY + ( dX - cX ) * sinf( _rz ) + ( dY - cY ) * cosf( _rz );

		render->D3DTexCoord2f( 1.0, 0.0 );
		render->D3DVertex2f( Xn, Yn );

		dX = W;
		dY = H;

		Xn = X + cX + ( dX - cX ) * cosf( _rz ) - ( dY - cY ) * sinf( _rz );
		Yn = Y + cY + ( dX - cX ) * sinf( _rz ) + ( dY - cY ) * cosf( _rz );

		render->D3DTexCoord2f( 1.0, 1.0 );
		render->D3DVertex2f( Xn, Yn );

		render->End();
		return true;
	}

	return false;
}
#endif

void SRTexture::ReInit( const int width, const int height ) {
	if ( textureSize.x == width && textureSize.y == height ) return;
	Invalidate();
	safe_release( pTexture );
	textureSize = { width, height };
	// 	textureIsInit = false;
	PP1S = nullptr;
	Initialize();
}

void SRTexture::Begin() {
	if ( isReleased ) { return; }

	if ( isRenderToTexture ) { return; }

#if __has_include( <d3dx9.h>)
	if ( !SUCCEEDED( render->BeginRender() ) ) { return; }
#endif

	pDevice->GetRenderTarget( 0, &OldRT );
	pDevice->GetDepthStencilSurface( &OldDS );

	pTexture->GetSurfaceLevel( 0, &PP1S ); // КАКОГО ХУЯ ОНО КРАШИТ?
	PP1S->Release();

	pDevice->SetDepthStencilSurface( DS );
	pDevice->SetRenderTarget( 0, PP1S );

	isRenderToTexture = true;
	isRendered = false;
}

void SRTexture::End() {
	if ( isReleased ) { return; }

	if ( !isRenderToTexture ) { return; }

	pDevice->SetRenderTarget( 0, OldRT );
	pDevice->SetDepthStencilSurface( OldDS );

	safe_release( OldRT );
	safe_release( OldDS );

#if __has_include( <d3dx9.h>)
	render->EndRender();
#endif
	isRenderToTexture = false;
	isRendered = true;
}

bool SRTexture::Clear() {
	if ( isReleased ) { return false; }

	if ( !isRenderToTexture ) { return false; }

	pDevice->Clear( 0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0, 1.0f, 0 );

	return true;
}

HRESULT SRTexture::Save( std::string_view fileName ) {
	if ( isReleased ) { return E_FAIL; }

	return D3DXSaveTextureToFileA( fileName.data(), D3DXIFF_JPG, pTexture, NULL );
}

HRESULT SRTexture::Save( ID3DXBuffer *buffer ) {
	if ( isReleased ) { return E_FAIL; }

	return D3DXSaveTextureToFileInMemory( &buffer, D3DXIFF_JPG, pTexture, NULL );
}

void SRTexture::setRotZ( float rz ) {
	_rz = rz;
}
float SRTexture::rotZ() {
	return _rz;
}

#if __has_include( <d3dx9.h>)
bool SRTexture::Render( const int X, const int Y, int W, int H ) {
	if ( isRenderToTexture ) { return false; }

	return Draw( pTexture, X, Y, W, H );
}
#endif

IDirect3DTexture9 *SRTexture::GetTexture() {
	if ( isReleased ) {
		// throw "Texture is released";
		return nullptr;
	}

	return this->pTexture;
}

POINT SRTexture::GetSize() {
	return textureSize;
}

bool SRTexture::IsRendered() {
	return isRendered;
}

bool SRTexture::IsRenderToTexture() {
	return isRenderToTexture;
}
