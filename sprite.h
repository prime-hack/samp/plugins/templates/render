#ifndef SRSPRITE_H
#define SRSPRITE_H

#if __has_include( <d3dx9.h>)

#	include "color.h"

#	ifdef _MSVC_VER
struct hookIDirect3DDevice9;
struct IDirect3DTexture9;
#	else
class hookIDirect3DDevice9;
class IDirect3DTexture9;
#	endif
struct D3DXMATRIX;
struct D3DXVECTOR3;

class SRSprite {
	struct rect_t {
		int left = 0;
		int top = 0;
		int right = 0;
		int bottom = 0;
		bool empty() const { return !left && !top && !right && !bottom; }
	};

	hookIDirect3DDevice9 *_dev = nullptr;
	class DrawHook *_drw = nullptr;
	class ID3DXSprite *_sprt = nullptr;

	int _prr = -1, _por = -1;

	SRColor _color = eCdWhite;
	rect_t _rect;

public:
	SRSprite() = delete;
#	if __has_include( <ProxyDX9/ProxyDX9.h>)
	SRSprite( hookIDirect3DDevice9 *device );
#	endif
#	if __has_include( <DrawHook/DrawHook.h>)
	SRSprite( DrawHook *draw );
#	endif
	~SRSprite();

	void ResetColor();
	void SetColor( SRColor color );
	SRColor Color() const;

	void ResetRect();
	void SetRect( const rect_t rect );
	const rect_t &Rect() const;

	void Show( IDirect3DTexture9 *texture, const D3DXMATRIX *mat ) const;
	void Show( IDirect3DTexture9 *texture, const D3DXVECTOR3 *pos ) const;
	void Show( IDirect3DTexture9 *texture, int x, int y, int w, int h, float r = 0.0f ) const;
	void Show( IDirect3DTexture9 *texture, int x, int y, float r = 0.0f ) const;
};

#endif

#endif // SRSPRITE_H
