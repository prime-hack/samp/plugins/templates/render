/*

	PROJECT:		mod_sa
	LICENSE:		See LICENSE in the top level directory
	COPYRIGHT:		Copyright we_sux, BlastHack

	mod_sa is available from https://github.com/BlastHackNet/mod_s0beit_sa/

	mod_sa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	mod_sa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with mod_sa.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef _D3DRENDER_H
#define _D3DRENDER_H

#if __has_include( <d3dx9.h>)

#	include "color.h"
#	include <d3d9.h>
#	include <string_view>
#	include <vector>

class CD3DFont;
class CD3DRender;

#	define FCR_NONE 0x0
#	define FCR_BOLD 0x1
#	define FCR_ITALICS 0x2
#	define FCR_BORDER 0x4

// FVF Macros
#	define D3DFVF_BITMAPFONT ( D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1 )
#	define D3DFVF_PRIMITIVES ( D3DFVF_XYZRHW | D3DFVF_DIFFUSE )

typedef struct d3dvertex_s {
	float x, y, z, rhw;
	DWORD color;
	float tu, tv;
} d3dvertex_s;

typedef struct d3dvertex3D_s {
	float x, y, z;
	DWORD color;
	float tu, tv;
} d3dvertex3D_s;

// Initialization Macros
#	undef SAFE_RELEASE
#	define SAFE_RELEASE( d ) \
		if ( d ) { \
			d->Release(); \
			d = NULL; \
		}

#	define D3DFVF_RESERVED1 0x020
#	define D3DFVF_LVERTEX ( D3DFVF_XYZ | D3DFVF_RESERVED1 | D3DFVF_DIFFUSE | D3DFVF_SPECULAR | D3DFVF_TEX1 )

typedef float D3DVALUE, *LPD3DVALUE;

typedef struct D3DLVERTEX {
	union {
		D3DVALUE x; /* Homogeneous coordinates */
		D3DVALUE dvX;
	};
	union {
		D3DVALUE y;
		D3DVALUE dvY;
	};
	union {
		D3DVALUE z;
		D3DVALUE dvZ;
	};
	DWORD dwReserved;
	union {
		D3DCOLOR color; /* Vertex color */
		D3DCOLOR dcColor;
	};
	union {
		D3DCOLOR specular; /* Specular component of vertex */
		D3DCOLOR dcSpecular;
	};
	union {
		D3DVALUE tu; /* Texture coordinates */
		D3DVALUE dvTU;
	};
	union {
		D3DVALUE tv;
		D3DVALUE dvTV;
	};
} D3DLVERTEX, *LPD3DLVERTEX;

class CD3DBaseRender {
public:
	CD3DBaseRender();
	~CD3DBaseRender();

	static HRESULT BeginRender();
	static HRESULT EndRender();

protected:
	static HRESULT Initialize( class IDirect3DDevice9 *pD3Ddev );
	static HRESULT Invalidate();

	static HRESULT CreateStates();
	static HRESULT DeleteStates();

	static class IDirect3DDevice9 *m_pD3Ddev;
	static class IDirect3DStateBlock9 *m_pD3DstateDraw;
	static class IDirect3DStateBlock9 *m_pD3DstateNorm;

	static int m_renderCount;
	static int m_numShared;
	static bool m_statesOK;
};

struct stColorGrad {
	SRColor lu;
	SRColor ru;
	SRColor lb;
	SRColor rb;

	stColorGrad( SRColor lu, SRColor ru, SRColor lb, SRColor rb ) : lu( lu ), ru( ru ), lb( lb ), rb( rb ) {}
	stColorGrad( SRColor u, SRColor b ) : lu( u ), ru( u ), lb( b ), rb( b ) {}
};

class CD3DFont : public CD3DBaseRender {
public:
	CD3DRender *m_pRender;

	CD3DFont( std::string_view fontName, int fontHeight = 14, DWORD dwCreateFlags = 4, DWORD dwCharSet = DEFAULT_CHARSET );
	~CD3DFont();

	HRESULT Initialize( IDirect3DDevice9 *pD3Ddev );
	HRESULT Invalidate();

	HRESULT Print( float x, float y, std::string_view text, SRColor color = -1, bool skipColorTags = false, bool noColorFormat = false );
	HRESULT PrintU8( float x, float y, std::string_view text, SRColor color = -1, bool skipColorTags = false, bool noColorFormat = false );
	HRESULT PrintShadow( float x, float y, std::string_view text, DWORD color = -1, DWORD color_shadow = 0xFF000000 );
	HRESULT PrintShadowU8( float x, float y, std::string_view text, DWORD color = -1, DWORD color_shadow = 0xFF000000 );

	float DrawLength( std::string_view text, bool noColorFormat = false ) const;
	float DrawLengthU8( std::string_view text, bool noColorFormat = false ) const;
	size_t GetCharPos( std::string_view text, float x, bool noColorFormat = false ) const;
	size_t GetCharPosU8( std::string_view text, float x, bool noColorFormat = false ) const;

	float DrawHeight() const { return m_fChrHeight; };

	size_t BindColorGrad( const stColorGrad &grad );

	float lineOffset = 0.0f;
	float emptyLineSize = 5.0f;

private:
	HRESULT CreateVertexBuffers();
	HRESULT DeleteVertexBuffers();

	char m_szFontName[31 + 1];
	int m_fontHeight;
	DWORD m_dwCreateFlags;
	DWORD m_dwCharSet;

	bool m_isReady;
	bool m_isInit;

	IDirect3DTexture9 *m_pD3Dtex;
	IDirect3DVertexBuffer9 *m_pD3Dbuf;

	DWORD m_maxTriangles;

	int m_texWidth, m_texHeight;
	int m_chrSpacing;
	float m_fTexCoords[224][4];
	float m_fChrHeight;

	std::vector<stColorGrad> m_colGrad;
};

struct POINT3D {
	float x;
	float y;
	float z;
};

struct POINT3DQUAD {
	POINT3D lu;
	POINT3D ru;
	POINT3D lb;
	POINT3D rb;
};

class CD3DRender : public CD3DBaseRender {
public:
	CD3DRender( int numVertices );
	~CD3DRender();

	HRESULT Initialize( IDirect3DDevice9 *pD3Ddev );
	HRESULT Invalidate();

	HRESULT Begin( D3DPRIMITIVETYPE primType );
	HRESULT End();

	HRESULT D3DColor( SRColor color );
	void D3DBindTexture( IDirect3DTexture9 * );
	void D3DBindShader( IDirect3DPixelShader9 * );
	void D3DTexCoord2f( float u, float v );
	HRESULT D3DVertex2f( float x, float y );

	void D3DTexQuad( float sx,
					 float sy,
					 float ex,
					 float ey,
					 SRColor color = -1,
					 float su = 0.0f,
					 float sv = 0.0f,
					 float eu = 1.0f,
					 float ev = 1.0f );
	void D3DTexQuad3D( POINT3DQUAD pos, bool zEnable = true, float su = 0.0f, float sv = 0.0f, float eu = 1.0f, float ev = 1.0f );
	void D3DBox( float x, float y, float w, float h, SRColor color );
	void D3DBoxGrad( float x, float y, float w, float h, SRColor cl_lu, SRColor cl_ru, SRColor cl_lb, SRColor cl_rb );
	void D3DBoxi( int x, int y, int w, int h, SRColor color, int maxW );
	void D3DBoxBorder( float x, float y, float w, float h, SRColor border_color, SRColor color );
	void D3DBoxBorderi( int x, int y, int w, int h, SRColor border_color, SRColor color );
	void D3DBoxEmpty( int x, int y, int w, int h, float rot, SRColor color );
	void D3DLine( int x, int y, int x2, int y2, SRColor color );
	bool DrawLine( const struct D3DXVECTOR3 &a, const struct D3DXVECTOR3 &b, SRColor color );

private:
	D3DPRIMITIVETYPE m_primType;
	IDirect3DVertexBuffer9 *m_pD3Dbuf;
	d3dvertex_s *m_pVertex;

	DWORD m_color;
	float m_tu, m_tv;
	class IDirect3DTexture9 *m_texture;
	class IDirect3DPixelShader9 *m_shader;
	int m_maxVertex;
	int m_curVertex;

	bool m_canRender;
	bool m_isInit;
};
#endif

#endif
