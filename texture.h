#ifndef CCREATETEXTURE_H
#define CCREATETEXTURE_H

#include "color.h"
#include "d3drender.h"

/**
 * @brief Класс реализующий рисование на текстуре
 */
class SRTexture {
public:
	/**
	 * @brief Конструктор
	 * @param width ширина текстуры
	 * @param height высота текстуры
	 * @param parent родительский класс
	 */
	SRTexture( int width, int height );
	~SRTexture();

	/**
	 * @brief Начало рисования в текстуру
	 */
	void Begin();
	/**
	 * @brief Завершение рисования в текстуру
	 */
	void End();

	/**
	 * @brief Удаление всего с текстуры
	 * @param color цвет, которым будет залита текстура после очистки
	 * @return успешность выполнения
	 */
	bool Clear();
#if __has_include( <d3dx9.h>)
	/**
	 * @brief Рисование текстуры на экране
	 * @param X позиция
	 * @param Y позиция
	 * @param W ширина
	 * @param H высота
	 * @return успешность выполнения
	 */
	bool Render( int X, int Y, int W = 0, int H = 0 );
#endif
	/**
	 * @brief Сохранение текстуры в файл
	 * @param fileName название файла
	 * @return HRESULT
	 */
	HRESULT Save( std::string_view fileName );
	/**
	 * @brief Сохранение текстуры в память
	 * @param buffer буффер для текстуры в памяти
	 * @return HRESULT
	 */
	HRESULT Save( class ID3DXBuffer *buffer );

	/**
	 * @brief вращение текстуры по оси Z
	 * @detail поворачивает текстуру по часовой стрелке, беря за точку опоры левый верхний угол
	 * @param rz псевдоугол вращения
	 */
	void setRotZ( float rz );

	/**
	 * @brief вращение текстуры по оси Z
	 * @return псевдоугол
	 */
	float rotZ();

	/**
	 * @brief реинициализация текстуры
	 * @param width новая ширина текстуры
	 * @param height новая высота текстуры
	 */
	void ReInit( int width, int height );

	/// \return указатель на текстуру
	IDirect3DTexture9 *GetTexture();
	/// \return размер текстуры
	POINT GetSize();

	/// \return true, если текстура не пустая
	bool IsRendered();

	/**
	 * @brief Check is begined render to texture
	 * @return true, when render begined
	 */
	bool IsRenderToTexture();

	void Invalidate();
	void Initialize( IDirect3DDevice9 *pDevice = nullptr );

protected:
#if __has_include( <d3dx9.h>)
	bool Draw( IDirect3DTexture9 *texture, int X, int Y, int W = 0, int H = 0 );
#endif

	IDirect3DTexture9 *pTexture;
	POINT textureSize;
	IDirect3DDevice9 *pDevice;

	float _rz;

	bool isRenderToTexture;
	bool isReleased;
	bool isRendered;

private:
	LPDIRECT3DSURFACE9 PP1S;
	LPDIRECT3DSURFACE9 DS;
	LPDIRECT3DSURFACE9 OldRT, OldDS;

#if __has_include( <d3dx9.h>)
	CD3DRender *render;
#endif
};

#endif // CCREATETEXTURE_H
