#if __has_include( <d3dx9.h>)

#	include "sprite.h"
#	include "d3drender.h"
#	include <d3dx9.h>
#	if __has_include( <ProxyDX9/ProxyDX9.h>)
#		include <ProxyDX9/ProxyDX9.h>
#	endif
#	if __has_include( <DrawHook/DrawHook.h>)
#		include <DrawHook/DrawHook.h>
#	endif

#	if __has_include( <ProxyDX9/ProxyDX9.h>)
SRSprite::SRSprite( hookIDirect3DDevice9 *device ) : _dev( device ) {
	D3DXCreateSprite( _dev->d3d9_device(), &_sprt );

	_prr = _dev->onPreReset += [this]() {
		SAFE_RELEASE( _sprt );
	};
	_por = _dev->onPostReset += [this]() {
		D3DXCreateSprite( _dev->d3d9_device(), &_sprt );
	};
}
#	endif

#	if __has_include( <DrawHook/DrawHook.h>)
SRSprite::SRSprite( DrawHook *draw ) : _drw( draw ) {
	D3DXCreateSprite( _drw->d3d9_device(), &_sprt );

	_prr = _drw->onPreReset += [this]() {
		SAFE_RELEASE( _sprt );
	};
	_por = _drw->onPostReset += [this]() {
		D3DXCreateSprite( _drw->d3d9_device(), &_sprt );
	};
}
#	endif

SRSprite::~SRSprite() {
#	if __has_include( <ProxyDX9/ProxyDX9.h>)
	if ( _dev ) {
		_dev->onPostReset -= _por;
		_dev->onPreReset -= _prr;
	}
#	endif
#	if __has_include( <DrawHook/DrawHook.h>)
	if ( _drw ) {
		_drw->onPostReset -= _por;
		_drw->onPreReset -= _prr;
	}
#	endif
	SAFE_RELEASE( _sprt );
}

void SRSprite::ResetColor() {
	_color = eCdWhite;
}

void SRSprite::SetColor( SRColor color ) {
	_color = color;
}

SRColor SRSprite::Color() const {
	return _color;
}

void SRSprite::ResetRect() {
	_rect = { 0, 0, 0, 0 };
}

void SRSprite::SetRect( const SRSprite::rect_t rect ) {
	_rect = rect;
}

const SRSprite::rect_t &SRSprite::Rect() const {
	return _rect;
}

void SRSprite::Show( IDirect3DTexture9 *texture, const D3DXMATRIX *mat ) const {
	if ( !_sprt ) return;
	if ( !texture ) return;
	_sprt->Begin( D3DXSPRITE_ALPHABLEND );
	_sprt->SetTransform( mat );
	if ( _rect.empty() )
		_sprt->Draw( texture, NULL, NULL, NULL, _color.argb );
	else
		_sprt->Draw( texture, (RECT *)&_rect, NULL, NULL, _color.argb );
	_sprt->End();
}

void SRSprite::Show( IDirect3DTexture9 *texture, const D3DXVECTOR3 *pos ) const {
	if ( !_sprt ) return;
	if ( !texture ) return;
	_sprt->Begin( D3DXSPRITE_ALPHABLEND );
	_sprt->SetTransform( NULL );
	if ( _rect.empty() )
		_sprt->Draw( texture, NULL, NULL, pos, _color.argb );
	else
		_sprt->Draw( texture, (RECT *)&_rect, NULL, pos, _color.argb );
	_sprt->End();
}

void SRSprite::Show( IDirect3DTexture9 *texture, int x, int y, int w, int h, float r ) const {
	if ( !_sprt ) return;
	if ( !texture ) return;
	if ( !w || !h ) return;

	D3DSURFACE_DESC desc;
	texture->GetLevelDesc( 0, &desc );

	D3DXMATRIX mat;
	D3DXVECTOR2 axisPos = D3DXVECTOR2( x, y );
	D3DXVECTOR2 size( ( 1.0f / desc.Width ) * w, ( 1.0f / desc.Height ) * h );
	D3DXVECTOR2 axisCenter = D3DXVECTOR2( w / 2.0f, h / 2.0f );

	D3DXMatrixTransformation2D( &mat, NULL, 0.0f, &size, &axisCenter, r, &axisPos );

	return Show( texture, &mat );
}

void SRSprite::Show( IDirect3DTexture9 *texture, int x, int y, float r ) const {
	if ( !_sprt ) return;
	if ( !texture ) return;

	D3DSURFACE_DESC desc;
	texture->GetLevelDesc( 0, &desc );

	return Show( texture, x, y, desc.Width, desc.Height, r );
}

#endif
